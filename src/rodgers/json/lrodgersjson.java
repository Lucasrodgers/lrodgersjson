package rodgers.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class lrodgersjson {

    //This takes in the User Login and turns it into JSON text
    public static String userLoginToJSON(UserLogin userLogin) {
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonText = "";

        try {
            //Pretty Printer makes the JSON text look more readable
            jsonText = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(userLogin);
        }catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return jsonText;
    }
    //This takes User Login and turns it into a string
    public static UserLogin JSONToText(String text) {
        
        ObjectMapper objectMapper = new ObjectMapper();
        UserLogin userLogin = null;

        try {
            userLogin = objectMapper.readValue(text, UserLogin.class);
        }catch (JsonProcessingException e){
            System.err.println(e.toString());
        }
        return userLogin;
    }

    public static void main(String[] args) {
        //Creates two instances of userLogin
        UserLogin newUser = new UserLogin();
        UserLogin anotherUser = new UserLogin();

        //Sets the info for each person
        newUser.setUsername("John_Bridges_Porter");
        newUser.setPassword("Pa55w0Rd2020");
        newUser.setThreeDigitPin(403);
        newUser.setBackupEmail("johnbridges@JBPIndustries.com");

        anotherUser.setUsername("Kimberly_Jones");
        anotherUser.setPassword("h3f%faEsf23");
        anotherUser.setThreeDigitPin(324);
        anotherUser.setBackupEmail("KJones@JBPIndustries.com");


        System.out.println("\n");
        String jsonJohn = lrodgersjson.userLoginToJSON(newUser);
        String jsonKim = lrodgersjson.userLoginToJSON(anotherUser);
        UserLogin loginJohn = lrodgersjson.JSONToText(jsonJohn);
        UserLogin loginKim = lrodgersjson.JSONToText(jsonKim);


        System.out.println("JOHN'S INFORMATION\n\n" + jsonJohn
                + "\n" + loginJohn);

        System.out.println("KIMBERLY'S INFORMATION\n\n" + jsonKim
                + "\n" + loginKim);
        return;
    }
}
