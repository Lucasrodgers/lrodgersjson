package rodgers.json;

public class UserLogin {



    private String username;
    private String password;
    private Integer threeDigitPin;
    private String backupEmail;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getThreeDigitPin() {
        return threeDigitPin;
    }

    public String getBackupEmail() {
        return backupEmail;
    }

    public void setBackupEmail(String backupEmail) {
        this.backupEmail = backupEmail;
    }

    public void setThreeDigitPin(Integer threeDigitPin) {
        this.threeDigitPin = threeDigitPin;
    }
    public String toString(){
        return "\nYour username is " + username + "\nYour password is "
                + password + "\nYour 3-digit pin is " + threeDigitPin
                + "\nYour back up email is " + backupEmail + "\n";
    }
}
